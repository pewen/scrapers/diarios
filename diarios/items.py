# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# https://docs.scrapy.org/en/latest/topics/items.html

import scrapy


class NewsItem(scrapy.Item):
    volanta = scrapy.Field()
    title = scrapy.Field()
    bajada = scrapy.Field()
    fecha = scrapy.Field()
    categorias = scrapy.Field()
    tags = scrapy.Field()
    autor = scrapy.Field()
    cuerpo = scrapy.Field()
    url = scrapy.Field()

    id_nota = scrapy.Field()

class DiariosItem(scrapy.Item):
    # define the fields for your item here like:
    # name = scrapy.Field()
    pass
