import json
from urllib.parse import quote, urlencode

import scrapy
from bs4 import BeautifulSoup

def create_url(page_url, next_start=None):
    """Contruye el url para pedir los comentarios.

    Params:
    -------
    page_url (str): url a la noticia.
    next_start (str): paginado para traer los proximos comentarios.
    """
    params = {
            'APIKey': '2_fq_ZOJSR4xNZtv2rA8DALl1Gxp7yTYMb3UdER6zerupB55mwkzh9pVBz4Blzi8SW',
            'authMode': 'cookie',
            'callback': 'gigya.callback',
            'categoryID': 'Com_03',
            'context': 'R3074394928',
            'ctag': 'comments_v2',
            'format': 'json',
            'includeSettings': 'false',
            'includeStreamInfo': 'false',
            'includeUserHighlighting': 'false',
            'includeUserOptions': 'false',
            'lang': 'es',
            'pageURL': '',
            'sdk': 'js_latest',
            'source': 'showCommentsUI',
            'sourceData': '{"categoryID":"Com_03","streamID":"Xip8mCsY"}',
            'streamID': '',
            'threaded': 'true'
        }
    # el final del url
    params["streamID"] = page_url.rsplit("_", maxsplit=1)[1].split(".")[0]
    params["pageURL"] =  quote(page_url, safe="")
    if next_start:
        params["start"] = next_start

    url = f"https://login.clarin.com/comments.getComments?{urlencode(params)}"
    return url


def parse_comment(c):
    """Extrae la info de un comentario.
    Parsea sus sub_comentarios de forma recursiva.
    """

    out = {
        "is_moderator": c["isModerator"],
        "comment_text": BeautifulSoup(c["commentText"].replace("\xa0", " "), "lxml").text,
        "pos_votes": c["posVotes"],
        "neg_votes": c["negVotes"],
        "flag_count": c["flagCount"],
        "sender_name": c["sender"]["name"]
    }

    if "loginProvider" in c["sender"]:
        out["sender_login_provider"] = c["sender"]["loginProvider"]

    if "replies" in c:
        out["replies"] = [parse_comment(i) for i in c["replies"]]
    return out


class ClarinCommentarios(scrapy.Spider):
    name = "clarin_comentarios"

    def start_requests(self):

        path_links = getattr(self, "path_links", None)
        with open(path_links) as f:
            for i, news_url in enumerate(f):
                url = create_url(news_url)
                yield scrapy.Request(url, cb_kwargs=dict(news_url=news_url))

    def parse(self, response, news_url=None, comments=None):
        data = json.loads(response.text)
        if not comments:
            comments = [parse_comment(c) for c in data["comments"]]
        else:
            comments += [parse_comment(c) for c in data["comments"]]
        next_start = data["next"]
        has_more = data["hasMore"]

        if has_more:
            url = create_url(news_url, next_start)
            yield scrapy.Request(url, cb_kwargs=dict(news_url=news_url,
                                                     comments=comments))

        else:
            yield {"news": news_url,
                   "comments": comments}
