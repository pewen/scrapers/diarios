import scrapy

from diarios.items import NewsItem
from diarios.utils import clean_weird_unicode


class LaNacionSpider(scrapy.Spider):
    name = "la_nacion"

    def start_requests(self):

        path_links = getattr(self, "path_links", None)
        with open(path_links) as f:
            for i, news_url in enumerate(f):
                yield scrapy.Request(news_url)

    def parse(self, response):
        item = NewsItem()
        item["url"] = response.url

        title = response.xpath("//h1/text()").get("").strip()
        item["title"] = clean_weird_unicode(title)

        fecha = response.xpath("//section[@class='fecha']")
        item["fecha"] = fecha.xpath("string()").get("").replace("\xa0", "")

        tags = response.xpath("//div[@class='breadcrumb']//a/text()").getall()
        item["categorias"] = " | ".join(elem.strip() for elem in tags)

        item["autor"] = response.xpath("//section[@class='autor']/a/text()").get("")

        chunks = []
        cuerpo = response.xpath("//section[@id='cuerpo']")
        for elem in cuerpo.xpath("p|h2"):
            tag = elem.root.tag
            if tag == "p":
                chunk = elem.xpath("string(.)").get("").strip()
                if chunk:
                    chunks.append(chunk)
            elif tag == "h2":
                chunk = elem.xpath("text()").get("").strip()
                if chunk:
                    chunks.append(chunk)

        cuerpo =  "\n".join(chunks).replace("\r\n", "")
        item["cuerpo"] = clean_weird_unicode(cuerpo)

        # id_nota para sacar los comentarios
        item["id_nota"] = response.xpath("//div[@id='tokenLF']/@data-entrada").get("")

        yield item
