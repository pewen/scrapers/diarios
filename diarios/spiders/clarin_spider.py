from itertools import cycle
from datetime import datetime

import scrapy

from diarios.items import NewsItem
from diarios.utils import clean_weird_unicode


class ClarinSpider(scrapy.Spider):
    name = "clarin"

    def start_requests(self):

        path_links = getattr(self, "path_links", None)
        with open(path_links) as f:
            for i, news_url in enumerate(f):
                yield scrapy.Request(news_url)


    def parse(self, response):
        item = NewsItem()
        item["url"] = response.url

        div_title = response.xpath("//div[@class='title']")
        item["volanta"] = div_title.xpath("p/text()").get("")
        title = div_title.xpath("h1/text()").get("")
        item["title"] = clean_weird_unicode(title)
        item["bajada"] = div_title.xpath("div/h2/text()").get("").strip()

        div_breadcrumb = response.xpath("//div[@class='breadcrumb col-lg-6 col-md-12 col-sm-12 col-xs-12']")
        item["fecha"] = div_breadcrumb.xpath("span/text()").get("").strip()

        item["categorias"] = " | ".join(div_breadcrumb.xpath(".//span[@itemprop='name']/text()").getall())
        item["tags"] = " | ".join(response.xpath("//div[@class='entry-tags']//h3/a/text()").getall())
        item["autor"] = response.xpath("//p[@itemprop='author']/text()").get("")
        p_news = response.xpath("//div[@class='body-nota']/p")
        item["cuerpo"] = "\n".join(p.strip() for p in p_news.xpath("string()").getall() if p)
        yield item
