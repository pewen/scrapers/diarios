def clean_weird_unicode(text: str) -> str:
    for u in ["\u0085", "\u0091", "\u0092", "\u0093",
              "\u0094", "\u0095", "\u0096", "\u0097"]:
        text = text.replace(u, "")

    return text
