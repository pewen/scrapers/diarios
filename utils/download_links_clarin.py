"""Util para bajar los links de clarin desde los xml del sitemap por años."""
from pathlib import Path

import requests
from bs4 import BeautifulSoup


path_out = Path("links/clarin")
path_out.mkdir(parents=True, exist_ok=True)

START_YEAR = 2019
END_YEAR = 1997

END_YEAR -= 1 

if START_YEAR > END_YEAR:
    step = -1
else:
    step = 1

for y in range(START_YEAR, END_YEAR, step):
    p = path_out / f"links_clarin_{y}.txt"
    for m in range(12, 0, -1):
        m = str(m)
        if len(m) == 1:  # llenamos con un cero
            m = "0" + m
            
        url = f"https://www.clarin.com/contents/sitemap_news_{y}_{m}.xml"
        count_items = 0
        while not count_items:
            r = requests.get(url)
            soup = BeautifulSoup(r.text, "lxml")
            count_items = len([l.text for l in soup.find_all("loc")])
        
        print(f"bajando: {url}")
        print(f"tenemos {count_items} links")
        with p.open("a") as f:
            for url in (l.text for l in soup.find_all("loc")):
                f.write(url + '\n')