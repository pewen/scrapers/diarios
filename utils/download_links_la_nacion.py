"""Util para bajar los links de la nacion desde los xml del sitemap por años."""
from pathlib import Path
from urllib.parse import urljoin

import requests
from bs4 import BeautifulSoup


path_out = Path("links/la_nacion")
path_out.mkdir(parents=True, exist_ok=True)

start_url = "https://www.lanacion.com.ar/notas-archivo.xml"
r = requests.get(start_url)
soup = BeautifulSoup(r.text, "lxml")
xml_links = [l.text for l in soup.find_all("loc")]

for xml_link in xml_links:
    count_items = 0
    y = xml_link.rsplit("-", 4)[1]
    p = path_out / f"links_la_nacion_{y}.txt"
    print(f"link: {xml_link}")
    r = requests.get(xml_link)
    soup = BeautifulSoup(r.text, "lxml")
    count_items = len([l.text for l in soup.find_all("loc")])
    print(f"count: {count_items}")
    with p.open("a") as f:
        if count_items:
            for url in (l.text for l in soup.find_all("loc")):
                f.write(url + '\n')
        else:  # caso raro
            count_items = len([urljoin(nota.attrs["dominio"],
                                nota.attrs["nota_url"]) for nota in soup.find_all("nota")])
            print(f"caso raro, real count: {count_items}")
            for url in (urljoin(nota.attrs["dominio"],
                                nota.attrs["nota_url"]) for nota in soup.find_all("nota")):
                f.write(url + '\n')