# Scraper Diarios

## [Clarin](https://www.clarin.com)

Desde el [sitemap_index](https://www.clarin.com/sitemap_index.xml).

### Comentarios
Desde el json de la api de los comentarios.

## [La nacion](https://www.lanacion.com.ar)

Desde sitemap [notas-archivo](https://www.lanacion.com.ar/notas-archivo.xml).

### Notas

Como las corridas son largas, se le puede agregar `-s JOBDIR=crawls/spider_name` para reanudarlas si se cortan.

## TODO

- Dockerizar las arrañas
- [X] Bajar solo las noticias de un solo mes.
- Diarios scrapers
  - [X] La nacion scraper.
    - [X] Limpiar el unicode raro que mete en las noticias
    - [X] Sacar el id de la nota para conseguir los conmentarios.
    - [X] Sacar el autor.
    - Sacar img principal.
  - [X] Clarin scraper.
    - [X] Sacar los tags de la noticia.
    - [X] Sacar el autor.
    - Sacar imagen principal.
    - [X] Asegurarse de sacar controls caracteres (unicode raro) en el titulo.
    - [X] Redidigir en los 301.
  - Pagina 12 scraper.
- Scrapear los comentarios de las noticias.
  - [X] Scraper comentarios clarin.
    - [X] Pasarle entrada como parametro.
    - Bug: algunas pocas veces da `KeyError: 'comments'` en la linea 83 y algunos timeout (posiblemente solo alcance con reintentar).
    - Warning bs4: UserWarning: "b'..'" looks like a filename, not markup. You should probably open this file and pass the filehandle into Beautiful Soup.
- Comprobar que esten todo bajado del sitemap (cantidad de item en clarin sea la cantidad de cosas en el xml sitemap)
- Comprobar que las noticias tengan titulo y cuerpo.
- Hacer dumps como archivo de noticias.
- Utils
  - [X] Para la nacion algunos xml del sitemap son diferentes (notas-2016-11.xml, y notas-2013-10.xml).
- Bajar del cache de google las noticias que no existen mas.
- Limpiar noticias con poca informacion (noticias cortas, y noticias que sean muy parecidas entre ellas como la quiniela).
